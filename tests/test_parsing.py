from pathlib import Path

import pytest

from openaip import iter_file


@pytest.fixture
def sample():
    with Path(__file__).parent.joinpath('sample.xml').open() as f:
        return list(iter_file(f))


def test_iter_file(sample):
    assert len(sample) == 2
    asp = sample[0]
    assert asp.category == "DANGER"
    assert asp.country == "FR"
    assert asp.name == "7 D 187 BourgStMaurice"
    assert asp.coordinates == [
        (6.6333333333333, 45.659166666667),
        (6.7127777777778, 45.697777777778),
        (6.745, 45.673888888889),
        (6.7525, 45.614444444444),
        (6.75, 45.613333333333),
        (6.7205555555556, 45.610833333333),
        (6.6333333333333, 45.659166666667)]


def test_as_geojson(sample):
    asp = sample[0]
    assert asp.as_geojson() == {
        'geometry': {
            'coordinates': [[
                (6.6333333333333, 45.659166666667),
                (6.7127777777778, 45.697777777778),
                (6.745, 45.673888888889),
                (6.7525, 45.614444444444),
                (6.75, 45.613333333333),
                (6.7205555555556, 45.610833333333),
                (6.6333333333333, 45.659166666667)]],
            'type': 'Polygon'
        },
        'properties': {
            'bottom': {'reference': 'GND', 'unit': 'F', 'value': 0},
            'category': 'DANGER',
            'country': 'FR',
            'id': '154870',
            'name': '7 D 187 BourgStMaurice',
            'top': {'reference': 'STD', 'unit': 'FL', 'value': 55}
        },
        'type': 'Feature'
    }
