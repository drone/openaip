import ujson as json
from minicli import cli, run

from . import as_geojson


@cli
def togeojson(filepath):
    print(json.dumps(as_geojson(filepath)))


if __name__ == '__main__':
    run()
