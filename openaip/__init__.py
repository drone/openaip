from functools import lru_cache
from lxml import etree


class Altitude:
    __slots__ = ['reference', 'unit', 'value', 'type']

    def __init__(self, raw):
        self.reference = raw.get('REFERENCE')
        self.unit = raw.find('ALT').get('UNIT')
        self.value = int(float(raw.find('ALT').text))
        self.type = 'top' if raw.tag == 'ALTLIMIT_TOP' else 'bottom'

    def __repr__(self):
        return '<Altitude {} {}/{}>'.format(self.value, self.unit,
                                            self.reference)

    def as_json(self):
        return {
            'reference': self.reference,
            'unit': self.unit,
            'value': self.value,
        }


class Airspace:

    __slots__ = ['_raw', 'id', 'name', 'country', 'category']

    def __init__(self, raw):
        self._raw = raw
        self.category = self._raw.get('CATEGORY')
        self.name = self._raw_value('NAME')
        self.country = self._raw_value('COUNTRY')
        self.id = self._raw_value('ID')

    def _raw_value(self, key):
        return self._raw.find(key).text

    def __repr__(self):
        return '<AirSpace "{}">'.format(self.name)

    @property
    @lru_cache(maxsize=1024)
    def coordinates(self):
        geom = self._raw.find('GEOMETRY/POLYGON')
        return [tuple(map(float, lonlat.strip().split()))
                for lonlat in geom.text.split(',')]

    @property
    @lru_cache(maxsize=1024)
    def bottom(self):
        return Altitude(self._raw.find('ALTLIMIT_BOTTOM'))

    @property
    @lru_cache(maxsize=1024)
    def top(self):
        return Altitude(self._raw.find('ALTLIMIT_TOP'))

    @property
    def properties(self):
        return {
            'id': self.id,
            'country': self.country,
            'name': self.name,
            'category': self.category,
            'bottom': self.bottom.as_json(),
            'top': self.top.as_json(),
        }

    def as_geojson(self):
        return {
            'type': 'Feature',
            'geometry': {
                'type': 'Polygon',
                'coordinates': [self.coordinates]
            },
            'properties': self.properties
        }


def iter_file(filepath):
    tree = etree.parse(filepath)
    root = tree.getroot()
    airspaces = root.find('AIRSPACES')
    for el in airspaces:
        yield Airspace(el)


def as_geojson(filepath):
    return {
        'type': 'FeatureCollection',
        'features': [a.as_geojson() for a in iter_file(filepath)]
    }
